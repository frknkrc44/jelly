package org.lineageos.jelly;

import android.Manifest;
import android.app.SearchManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import org.lineageos.jelly.utils.TabUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public class LaunchFileActivity extends AppCompatActivity {

    private String url;
    private Intent intent;
    private FileOutputStream fos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        intent = getIntent();

        if (intent != null && intent.getAction() != null) {
            if (intent.getAction().equals(Intent.ACTION_SEND)) {
                url = intent.getStringExtra(Intent.EXTRA_TEXT);
                if (url == null) {
                    urlCacheLocalUri((Uri) intent.getExtras().get(Intent.EXTRA_STREAM));
                    finish();
                }
                int indexOfUrl = url.toLowerCase().indexOf("http");
                if (indexOfUrl == -1)
                    finish();
                else {
                    String containsURL = url.substring(indexOfUrl);

                    int endOfUrl = containsURL.indexOf(" ");

                    if (endOfUrl != -1) {
                        url = containsURL.substring(0, endOfUrl);
                    } else {
                        url = containsURL;
                    }
                }
                TabUtils.openInNewTab(this, url, true);
            }else if (intent.getAction().equals(Intent.ACTION_PROCESS_TEXT)
                    && intent.getStringExtra(Intent.EXTRA_PROCESS_TEXT) != null){
                TabUtils.openInNewTab(this, intent.getStringExtra(Intent.EXTRA_PROCESS_TEXT), true);
            }else if (intent.getAction().equals(Intent.ACTION_WEB_SEARCH)
                    && intent.getStringExtra(SearchManager.QUERY) != null){
                TabUtils.openInNewTab(this, intent.getStringExtra(SearchManager.QUERY), true);
            }else if (intent.getBooleanExtra("kill_all", false)) {
                TabUtils.killAll(getApplicationContext());
            } else if (intent.getScheme() != null &&
                    (intent.getScheme().equals("content")
                            || intent.getScheme().equals("file"))) {
                if (intent.getScheme().equals("content")
                        || Objects.requireNonNull(intent.getDataString()).endsWith(".eml")
                        //|| (intent.getType().equals("message/rf822"))
                ) {
                    urlCacheLocalUri(intent.getData());
                } else {
                    url = intent.getDataString();
                }
                if (!hasStoragePermissionRead()) {
                    //finish();
                } else {
                    Toast.makeText(this, "permission READ_storage granted", Toast.LENGTH_LONG).show();
                }
                //TabUtils.openInNewTab(this, url, true);
            }
        }
        finish();
    }

    private void urlCacheLocalUri(Uri uri) {
        String sMime="";
        if (intent.getDataString()== null || !intent.getDataString().substring(intent.getDataString().lastIndexOf("/")).contains(".")) {
            if (getContentResolver().getType(uri) != null) {
                sMime = "."+MimeTypeMap.getSingleton().getExtensionFromMimeType(getContentResolver().getType(uri));
                if (sMime.equals(".bin")) sMime = mimeHead(uri);
                else if (sMime.equals(".null")) {
                    int i = Objects.requireNonNull(getContentResolver().getType(uri)).indexOf("/");
                    // if (i==0) sMime = "."; else
                    sMime = "." + Objects.requireNonNull(getContentResolver().getType(uri)).substring(i+1);
                    if (sMime.equals(".*")) sMime = mimeHead(uri);
                } else if (sMime.equals(".eml")) {
                    sMime = ".txt";
                }
            } else sMime = mimeHead(uri);
        } else if (intent.getDataString().endsWith(")")) sMime = mimeHead(uri);
        if (uri.toString().endsWith(".eml") || mimeHead(uri).equals(".eml")) {
            sMime = ".txt";
        }

        File f = new File(getBaseContext().getCacheDir(), uri.getLastPathSegment().replace(":", "").replace("/", ".")
                + sMime);
        try {
            fos = new FileOutputStream(f);
            InputStream input = getBaseContext().getContentResolver().openInputStream(uri);

            byte[] buffer = new byte[1024 * 4];
            int n = 0;
            while (-1 != (n = input.read(buffer))) {
                fos.write(buffer, 0, n);
            }
        } catch (IOException | NullPointerException e) {
            //Log.e("errro", e.toString());
        }
        url = "file:///" + f.getPath();

    }

    private String mimeHead(Uri uri) {
        try {
            byte[] buffer = new byte[1024];
            getBaseContext().getContentResolver().openInputStream(uri).read(buffer);
            String sHead = new String(buffer, StandardCharsets.UTF_8);
            if (sHead.contains("\nContent-Transfer-Encoding: quoted-printable"))
                return ".mht";
            if (sHead.contains("\nContent-Transfer-Encoding: binary"))
                return ".htm";
            if (sHead.contains("\nContent-Type: multipart/mixed;"))
                return ".eml";

            sHead = sHead.toUpperCase();
            if (sHead.startsWith("<!DOCTYPE HTML"))
                return ".htm";
            if (sHead.startsWith("<?XML") && sHead.contains("\n<SVG"))
                return ".svg";
            if (sHead.startsWith("<?XML"))
                return ".xml";
            if (sHead.contains("\n<!DOCTYPE HTML"))
                return ".htm";
        } catch (IOException | NullPointerException e) {
            //Log.e("errro", e.toString());
            return e.toString();
        }
        return ".";
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "permission validation", Toast.LENGTH_LONG).show();
            TabUtils.openInNewTab(this, url, true);
        } else {
            Toast.makeText(this, "permission READ_storage DENIED", Toast.LENGTH_LONG).show();
            ActivityCompat.finishAffinity(this);
        }
    }

    private boolean hasStoragePermissionRead() {
        if (ContextCompat.checkSelfPermission(this,android.Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)){
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }else ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            return false;
        } else {
            TabUtils.openInNewTab(this, url, true);
        } return true;
    }
}
